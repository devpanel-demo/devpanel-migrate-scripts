#!/bin/bash
# ---------------------------------------------------------------------
# DevPanel Migration Scripts
# Copyright (C) 2021 DevPanel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------

RED="\e[31m"
GREEN="\e[32m"
ENDCOLOR="\e[0m"

echo -e "\e[94m"
echo -e "-------------------------------------------"
echo -e "| DevPanel Migration: DevPanel to Bitnami |"
echo -e "-------------------------------------------"
echo -e "$ENDCOLOR"

# Functions
urlencode() {
  [[ "$@" ]] && printf "$@" | php -r "echo trim(urlencode(fgets(STDIN)));"
}

# Some first check
echo -e "> Run some first check"
if [ ! -f "$(which php)" ]; then
  echo -e "${RED}Error${ENDCOLOR}: Not found php-cli, this script need php-cli to work"
  exit 1
fi

# Variables
GIT_REPO_PATH=$(urlencode $1)
WORKDIR=/tmp/devpanel-migration
DRUPAL_DIR=/home/bitnami/stack/drupal
DATABASE_URL="https://gitlab.com/api/v4/projects/${GIT_REPO_PATH}/repository/files/$(urlencode '.devpanel/dumps/db.sql.tgz')/raw?ref=master"
STATIC_FILE_URL="https://gitlab.com/api/v4/projects/${GIT_REPO_PATH}/repository/files/$(urlencode '.devpanel/dumps/files.tgz')/raw?ref=master"
REPO_TARBALL_URL="https://gitlab.com/api/v4/projects/${GIT_REPO_PATH}/repository/archive.tar.gz"

if [ ! $GIT_REPO_PATH ]; then
  echo -e "${RED}Error${ENDCOLOR}: You need provide git repository"
  exit 1
fi

if [[ 200 != $(curl -o /dev/null -I -s -w "%{http_code}\n" "https://gitlab.com/api/v4/projects/${GIT_REPO_PATH}") ]]; then
  echo -e "${RED}Error${ENDCOLOR}: Cannot reach the git repository, make sure it exist or you have enought permission "
  exit 1;
fi

if [[ 200 != $(curl -o /dev/null -I -s -w "%{http_code}\n" $DATABASE_URL) ]]; then
  echo -e "${RED}Error${ENDCOLOR}: Cannot get the database, make sure it exist or you have enought permission "
  exit 1;
fi

if [[ 200 != $(curl -o /dev/null -I -s -w "%{http_code}\n" $STATIC_FILE_URL) ]]; then
  echo -e "${RED}Error${ENDCOLOR}: Cannot get the static files, make sure it exist or you have enought permission "
  exit 1;
fi

# Install needed tool
echo -e "> Install some needed tools"
if [[ ! -n $(which jq) ]]; then
  echo -e " -- jq not found. Installing jq"
  sudo apt update && sudo apt install -y jq
fi

if [[ ! -n $(which git) ]]; then
  echo -e " -- git not found. Installing git"
  sudo apt update && sudo apt install -y git
fi

#== Install Drush
composer global require drush/drush \
  && sudo ln -s /home/bitnami/.config/composer/vendor/bin/drush /usr/local/bin/drush

mkdir -p $WORKDIR && cd $WORKDIR

if [ ! -d "$DRUPAL_DIR" ]; then
  echo -e "${RED}Error${ENDCOLOR}: Drupal directory is not exist $DRUPAL_DIR"
  exit 1
fi

###########################
# BEGIN DATABASE JOBS
###########################
# Download database tgz and import

## Download database
cd $WORKDIR
echo -e "> Get the database"
curl -o db.sql.tgz $DATABASE_URL --silent

## Extract database
tar xzf "$WORKDIR/db.sql.tgz" -C $WORKDIR --transform 's/.*\.sql$/devpanel-drupal-db.sql/'

## Import database to drupal using drush
cd $DRUPAL_DIR
echo -e "> Drop current database"
drush sql-drop -y --quiet
echo -e "> Import database to Drupal"
drush sql-cli < $WORKDIR/devpanel-drupal-db.sql

###########################
# SOURCE CODE DATABASE JOBS
###########################
echo -e "> Backup the bitnami drupal stack"
sudo mv $DRUPAL_DIR $DRUPAL_DIR.bitnami
sudo mkdir -p $DRUPAL_DIR && sudo chown bitnami:daemon $DRUPAL_DIR

# Download source code tar ball
cd $WORKDIR
echo -e "> Get the sourcecode from git repo"
curl -o sourcecode.tar.gz $REPO_TARBALL_URL --silent

## Extract the source code
echo -e "> Extract the source code"
mkdir -p $WORKDIR/restore-source-code
tar xzf "$WORKDIR/sourcecode.tar.gz" -C $WORKDIR/restore-source-code --strip-components=1 --exclude='.devpanel' 

## Update composer.json and web-root to work with Bitnami Drupal apps
echo -e "> Modify the composer.json"
cd $WORKDIR/restore-source-code
SOURCECODE_WEBROOT=$(jq -r '."extra"."drupal-scaffold"."locations"."web-root"' composer.json)
SOURCECODE_WEBROOT_PATH=$(realpath -s $WORKDIR/restore-source-code/$SOURCECODE_WEBROOT)

# Change web-root to ./
jq '."extra"."drupal-scaffold"."locations"."web-root" = "./"' composer.json > tmp.$$.json && mv tmp.$$.json composer.json

# Modify the installer-paths
jq --arg SOURCECODE_WEBROOT "^$SOURCECODE_WEBROOT" '."extra"."installer-paths" |= with_entries(.key |= sub($SOURCECODE_WEBROOT; ""))' composer.json > tmp.$$.json && mv tmp.$$.json composer.json

# Update structure to right webroot
if [ -d "$SOURCECODE_WEBROOT_PATH" ]; then
  cp -r `ls -A $SOURCECODE_WEBROOT_PATH | xargs printf -- "$SOURCECODE_WEBROOT_PATH/%s\n"` $WORKDIR/restore-source-code/
fi

rm -fR $SOURCECODE_WEBROOT_PATH
sudo chown bitnami:daemon $WORKDIR/restore-source-code -R
cp -r `ls -A $WORKDIR/restore-source-code | xargs printf -- "$WORKDIR/restore-source-code/%s\n"` $DRUPAL_DIR

###########################
# Install composer
###########################
echo -e "> Run composer"
sudo rm -fR $DRUPAL_DIR/vendor
COMPOSER_MEMORY_LIMIT=-1 composer install -d $DRUPAL_DIR

###########################
# STATIC FILES JOBS
###########################

# Download file tgz
cd $WORKDIR
echo -e "> Get the static files"
curl -o files.tgz $STATIC_FILE_URL --silent

## Extract files to drupal
echo -e "> Extract static files to drupal dir"
mkdir -p $WORKDIR/restore-files
mkdir -p $DRUPAL_DIR/$(drush php-eval "echo \Drupal\Core\StreamWrapper\PublicStream::basePath();")

tar xzf "$WORKDIR/files.tgz" -C $WORKDIR/restore-files
sudo chown bitnami:daemon $WORKDIR/restore-files -R
cp -r `ls -A $WORKDIR/restore-files | xargs printf -- "$WORKDIR/restore-files/%s\n"` $DRUPAL_DIR/$(drush php-eval "echo \Drupal\Core\StreamWrapper\PublicStream::basePath();")


###########################
# CLEANUP
###########################
# Preverse settings files
sudo cp -r $DRUPAL_DIR.bitnami/sites/default/settings.php $DRUPAL_DIR/sites/default/settings.php
sudo chown bitnami:daemon $DRUPAL_DIR -fR

chmod 777 $DRUPAL_DIR/$(drush php-eval "echo \Drupal\Core\StreamWrapper\PublicStream::basePath();") -fR

echo -e "> Clear Drupal cache"
drush cr --quiet

echo -e "> Cleaning up"
rm -fR $WORKDIR

echo -e "> ${GREEN}Migration successful${ENDCOLOR}"
